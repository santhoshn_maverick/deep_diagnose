#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 24 22:13:26 2018

@author: santhosh
""" 
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import seaborn as sns
%matplotlib inline

df = pd.read_csv('Stats.csv')

""" Setting date_time as the index """

df.index = pd.to_datetime(df['timestamp'])
df.drop('timestamp', inplace=True, axis=1)
df.drop('Unnamed: 0', inplace=True, axis=1) 

""" Encoding the results to 1 and 0 """

label = LabelEncoder()
df['result'] = label.fit_transform(df['result'])

""" Creating new columns for positive and negative """

df['negative'] = df['result'] == 0
df['positive'] = df['result'] == 1

""" Function for replacing true by 1 and false as 0 """

def replace(x):
    if x == True:
        return 1
    else:
        return 0

df['negative'] = df['negative'].apply(lambda x : replace(x))
df['positive'] = df['positive'].apply(lambda x : replace(x)) 

df.drop('result', inplace=True, axis=1)

""" Generating Week wise data """

stats_week = df.resample('W').sum()

""" Generating Month wise data """

stats_month = df.resample('M').sum()

""" Generating 3 Months aggregated data """

stats_3_months = df.resample('3M').sum()

""" Generating Year wise data """

stats_year = df.resample('Y').sum() 

stats_3_months.plot(kind = 'bar', figsize = (10,6), stacked=True)
stats_year.plot(kind = 'bar', figsize = (10,6))
stats_week[:10].plot(kind = 'bar', figsize = (10,6))

stats_week.to_csv("week.csv")
stats_month.to_csv("month.csv")
stats_3_months.to_csv("3months.csv")
stats_year.to_csv("year.csv")
