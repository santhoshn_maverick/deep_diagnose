import os, time, datetime
from flask import Flask, render_template, request
import pandas as pd

app = Flask(__name__)

@app.route("/table", methods=['GET'])
def show_tables():
    df = pd.read_excel('stats.xlsx')
    return render_template('view.html',tables=[df.to_html()])

if __name__ == '__main__':
    app.debug = True
    host = os.environ.get('IP', '0.0.0.0')
    port = int(os.environ.get('PORT', 8080))
    app.run(host=host, port=port)
