import sys
import argparse
import numpy as np
from PIL import Image
import requests
from io import BytesIO

from keras.preprocessing import image
from keras.models import load_model
from keras.applications.inception_v3 import preprocess_input

def predict(model, img):
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    preds = model.predict(x)
    return preds

"""if __name__=="__main__":
    target_size = (299, 299)
    model = load_model("inception.model")
    img = image.load_img("cat.4001.jpg", target_size = (299, 299))
    preds = predict(model, img)"""

if __name__=="__main__":
    model = load_model("inception.model")
    img = image.load_img("dog.2.jpg", target_size = (299, 299))
    prediction = predict(model, img)
    if prediction[0][0] > 0.5:
        pred_class = 'cat'
    if prediction[0][1] > 0.5:
        pred_class = 'dog'
    print(pred_class)
