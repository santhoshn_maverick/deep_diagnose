import pygal as pg
import pandas as pd

df = pd.read_csv("week.csv")
df_m = pd.read_csv("month.csv")

df.index = df['timestamp']
df.drop('timestamp', inplace=True, axis=1)

df_m.index = df_m['timestamp']
df_m.drop('timestamp', inplace=True, axis=1)

bar_chart = pg.Bar()
bar_chart.title = 'Past 10 weeks trends'
bar_chart.x_labels = list(df.index.get_values()[44:54])
bar_chart.add('Positive', list(df['positive'][44:54]))
bar_chart.add('Negative', list(df['negative'][44:54]))
bar_chart.render_to_png('chart.png')

pie_chart = pg.Pie()
p = df['positive'][44:54].sum()
n = df['negative'][44:54].sum()
pie_chart.title = 'Past 6 weeks cases trend aggregated'
pie_chart.add('Positive', p)
pie_chart.add('Negative', n)

bar_chart_m = pg.Bar()
bar_chart_m.title = 'Past 6 months cases trend'
bar_chart_m.x_labels = list(df_m.index.get_values()[-6:])
bar_chart_m.add('Positive', list(df_m['positive'][-6:]))
bar_chart_m.add('Negative', list(df_m['negative'][-6:]))

pie_chart_m = pg.Pie()
p_m = df['positive'][-6:].sum()
n_m = df['negative'][-6:].sum()
pie_chart_m.title = 'Past 6 months cases trend aggregated'
pie_chart_m.add('Positive', p_m)
pie_chart_m.add('Negative', n_m)

df_y = pd.read_csv("year.csv")
df_y.index = df_y['timestamp']
df_y.drop('timestamp', inplace=True, axis=1)